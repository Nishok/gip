#include "CArea.h"

CArea CArea::AreaControl;

CArea::CArea() {
    AreaSize = 0;   //Set the area size to 0, so we don't get any problems later
}

bool CArea::OnLoad(char* File) {
    OnCleanup();    //Cleanup so we don't end up doubling the loadings

    FILE* FileHandle = fopen(File, "r");    //Open a file to read (r = read)

    if(FileHandle == NULL) {    //If file is not found, return false
        return false;
    }

    char TilesetFile[255];

    fscanf(FileHandle, "%s\n", TilesetFile);    //Scan through the file and save the %s into Tile and Type, %s means String of characters.

    if((Surf_Tileset = CSurface::OnLoad(TilesetFile)) == false) {   //If we can't load the tileset into a surface, do the following
        fclose(FileHandle); //Close the file reading

        return false;
    }

    fscanf(FileHandle, "%d\n", &AreaSize);

    for(int X = 0; X < AreaSize; X++) {
        for(int Y = 0; Y < AreaSize; Y++) {
            char MapFile[255];

            fscanf(FileHandle, "%s ", MapFile);

            CMap tempMap;

            if(tempMap.OnLoad(MapFile) == false) {
                fclose(FileHandle);

                return false;
            }

            tempMap.Surf_Tileset = Surf_Tileset;

            MapList.push_back(tempMap); //Increase the vector size by one, othewise we'll be getting errors
        }
        fscanf(FileHandle, "\n");   //Go to next line to read
    }

    fclose(FileHandle);

    return true;
}

void CArea::OnRender(SDL_Surface* Surf_Display, int CameraX, int CameraY) {
    int MapWidth = MAP_WIDTH * TILE_SIZE;	//Get the size of our map
    int MapHeight = MAP_HEIGHT * TILE_SIZE;

    int FirstID = -CameraX / MapWidth;
        FirstID = FirstID + ((-CameraY / MapHeight) * AreaSize);

    for(int i = 0; i < 4; i++) {
        int ID = FirstID + ((i / 2) * AreaSize) + (i % 2);

        if(ID < 0 || ID >= (int)MapList.size()) {
            continue;
        }

        int X = ((ID % AreaSize) * MapWidth) + CameraX;
        int Y = ((ID / AreaSize) * MapHeight) + CameraY;

        MapList[ID].OnRender(Surf_Display, X, Y);
    }
}

void CArea::OnCleanup() {
    if(Surf_Tileset) {
        SDL_FreeSurface(Surf_Tileset);  //Free the surface so it doesn't keep on using unnecesarry resources
    }

    MapList.clear();    //Clear the maplist
}

CMap* CArea::GetMap(int X, int Y) {
    int MapWidth  = MAP_WIDTH * TILE_SIZE;  //Get the size of our map
    int MapHeight = MAP_HEIGHT * TILE_SIZE;

    int ID = X / MapWidth;  //Retreive the ID of the areatile
        ID = ID + ((Y / MapHeight) * AreaSize);

    if(ID < 0 || ID >= (int)MapList.size()) {
        return NULL;
    }

    return &MapList[ID];
}

CTile* CArea::GetTile(int X, int Y) {
    int MapWidth  = MAP_WIDTH * TILE_SIZE;  //Get the size of the map
    int MapHeight = MAP_HEIGHT * TILE_SIZE;

    CMap* Map = GetMap(X, Y);

    if(Map == NULL) return NULL;

    X = X % MapWidth;   //Turn X and Y into tile coordinates
    Y = Y % MapHeight;

    return Map->GetTile(X, Y);  //Return the tile coordinates to the map
}

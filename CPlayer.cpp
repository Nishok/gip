#include "CPlayer.h"
#include "CProjectile.h"
#include "CSoundBank.h"
#include "CApp.h"

CPlayer::CPlayer() {
    X = 0;
    Y = 0;

    Width = 0;
    Height = 0;

    MoveLeft = false;
    MoveRight = false;
    MoveUp = false;
    MoveDown = false;

    CurrentFrameCol = 0;
    CurrentFrameRow = 2;

    Direction = 'D';

    Type = ENTITY_TYPE_PLAYER;

    MaxSpeedX = 7;
    MaxSpeedY = 7;

    Health = PLAYER_HEALTH;
    Kills = 0;
    Deaths = 0;

    Interval = 0;
    Interval1 = 0;
    Interval2 = 0;
    HPInterval = 0;
    WalkInterval = 10;
    HealthlossDelay = 2;
    AttackDelay = 10;
    HeartBeatDelay = 30;
}

bool CPlayer::OnLoad(char* File, int Width, int Height, int MaxFrames) {
    if(CEntity::OnLoad(File, Width, Height, MaxFrames) == false) {
        return false;
    }

    return true;
}

void CPlayer::OnLoop() {
    CEntity::OnLoop();
    if(MoveDown) {
        Direction = 'D';
        Walksound();
    }
    if(MoveUp) {
        Direction = 'U';
        Walksound();
    }
    if(MoveLeft) {
        Direction = 'L';
        Walksound();
    }
    if(MoveRight) {
        Direction = 'R';
        Walksound();
    }

    Interval += CFPS::FPSControl.GetSpeedFactor();
    Interval1 += CFPS::FPSControl.GetSpeedFactor();
    Interval2 += CFPS::FPSControl.GetSpeedFactor();
    HPInterval += CFPS::FPSControl.GetSpeedFactor();

    if(Health <= 2) {
        if(Interval1 > HeartBeatDelay) {
            CSoundBank::SoundControl.Play(app->SoundsB);
            Interval1 = 0;
        }
    }

    if(Health <= 0) {       //If the player dies, then he respawns back at the start, with the time still running and with full HP again.
        Health = PLAYER_HEALTH;
        X = PlayerSpawnX;
        Y = PlayerSpawnY;
    }
}

void CPlayer::OnRender(SDL_Surface* Surf_Display) {
    CEntity::OnRender(Surf_Display);
    if(Surf_Entity == NULL || Surf_Display == NULL) return;     //If we don't have anything to draw, then we skip this function

    CSurface::OnDraw(Surf_Display, Surf_Entity, X - CCamera::CameraControl.GetX(), Y - CCamera::CameraControl.GetY(), (CurrentFrameCol + Anim_Control.GetCurrentFrame())  * Width, (CurrentFrameRow) * Height, Width, Height);
}

void CPlayer::OnAnimate() {
    if(SpeedX != 0 || SpeedY != 0) {
        Anim_Control.MaxFrames = 3;     //If our speed is higher than 0, then the maxframes will be 3 (so you'll get the animation).
    }else{
        Anim_Control.MaxFrames = 0;     //If your speed is 0, then there will be no animation.
    }

    if(MoveLeft) {
        CurrentFrameRow = 3;    //Makes the animation look left
    }
    else if(MoveRight) {
        CurrentFrameRow = 1;     //Makes the animation look right
    }
    else if(MoveUp) {
        CurrentFrameRow = 0;     //Makes the animation look up
    }
    else if(MoveDown) {
        CurrentFrameRow = 2;     //Makes the animation look down
    }

    Anim_Control.OnAnimate();
}

bool CPlayer::OnCollision(CEntity* Entity) {
    if(Entity->Type == ENTITY_TYPE_ENEMY) {
        if(HPInterval > HealthlossDelay) {
            Health -= 1;
            Deaths += 1;
            HPInterval = 0;
        }
    }
    return true;
}

void CPlayer::OnCleanup() {
    CEntity::OnCleanup();
}

bool CPlayer::Fire() {
    if(Interval > AttackDelay) {
        CEntity::Fire();
        CSoundBank::SoundControl.Play(app->SoundsA);
        Interval = 0;
    }

    return true;
}

void CPlayer::Walksound() {
        if(Interval2 > WalkInterval) {
            CSoundBank::SoundControl.Play(app->SoundsC);
            Interval2 = 0;
        }
}


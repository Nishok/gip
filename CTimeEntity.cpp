#include "CTimeEntity.h"
#include "CApp.h"

CTimeEntity::CTimeEntity() {
    Flags = ENTITY_FLAG_PLYDETECT;
}

bool CTimeEntity::OnLoad(char* File, int Width, int Height, int MaxFrames) {
    if(CEntity::OnLoad(File, Width, Height, MaxFrames) == false) {
        return false;
    }
    return true;
}

void CTimeEntity::OnLoop() {
    CEntity::OnLoop();
}

void CTimeEntity::OnRender(SDL_Surface* Surf_Display) {
    CEntity::OnRender(Surf_Display);
    if(Surf_Entity == NULL || Surf_Display == NULL) return;     //If we don't have anything to draw, then we skip this function

    CSurface::OnDraw(Surf_Display, Surf_Entity, X - CCamera::CameraControl.GetX(), Y - CCamera::CameraControl.GetY(), (CurrentFrameCol + Anim_Control.GetCurrentFrame())  * Width, (CurrentFrameRow) * Height, Width, Height);
}

void CTimeEntity::OnCleanup() {
    CEntity::OnCleanup();
}

bool CTimeEntity::OnCollision(CEntity* Entity) {
    if(Entity->Type == ENTITY_TYPE_PLAYER) {
        //Flags = ENTITY_FLAG_MAPONLY;
        if(app->timerRunning == true) {
                app->timerRunning = false;
                //start = 0;
        }
    }

    return true;
}

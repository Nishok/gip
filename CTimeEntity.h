#ifndef _CTIMEENTITY_H_
    #define _CTIMEENTITY_H_

#include "CEntity.h"

class CTimeEntity : public CEntity {
    public:
        CTimeEntity();

		bool OnLoad(char* File, int Width, int Height, int MaxFrames);

        void OnLoop();

        void OnRender(SDL_Surface* Surf_Display);

        void OnCleanup();

        bool OnCollision(CEntity* Entity);
};

#endif

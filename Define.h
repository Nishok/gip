#ifndef _DEFINE_H_
    #define _DEFINE_H_

#define MAP_WIDTH 300 //40
#define MAP_HEIGHT 300 //40

#define TILE_SIZE 32

#define WWIDTH 640
#define WHEIGHT 480

#define FPS_MAX 60
#define WALKSPEED_MAX 5
#define NPC_FAST 6
#define NPC_MEDIUM 4
#define NPC_SLOW 2

#define PlayerSpawnX 4385
#define PlayerSpawnY 6150
#define PLAYER_HEALTH 5

#define ORC_AMOUNT 200

#endif

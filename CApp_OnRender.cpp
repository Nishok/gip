#include "CApp.h"

void CApp::OnRender() {

    SDL_Rect Rect;
	Rect.x = 0;
	Rect.y = 0;
	Rect.w = WWIDTH;
	Rect.h = WHEIGHT;

	SDL_FillRect(Surf_Display, &Rect, 0);
    //CArea::AreaControl.OnRender(Surf_Display, CCamera::CameraControl.GetX(), CCamera::CameraControl.GetY());
    CArea::AreaControl.OnRender(Surf_Display, -CCamera::CameraControl.GetX(), -CCamera::CameraControl.GetY());

    for(int i = 0;i < (int)CEntity::EntityList.size();i++) {     //To spawn the entities.
        if(!CEntity::EntityList[i]) continue;

        CEntity::EntityList[i]->OnRender(Surf_Display);
    }

    CTTF::TTF.OnRender(Surf_Display);

    SDL_Flip(Surf_Display); //This basically refreshes the buffer and displays Surf_Display onto the screen. If we don't, then the screen will be flikkering.
};

#ifndef _CFPS_H_
    #define _CFPS_H_

#include <SDL/SDL.h>
#include "Define.h"

class CFPS {
    private:
        int OldTime;    //To calculate the FPS
        int LastTime;   //To calculate the speedfactor
        int NumFrames;  //The current FPS
        int Frames;
        int MaxFrames;  //The maximum of frames

        float SpeedFactor;

    public:
        static CFPS FPSControl;

        CFPS();

        int GetFPS();

        float GetSpeedFactor();

        void OnLoop();
};

#endif

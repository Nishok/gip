#ifndef _CSOUNDBANK_H_
    #define _CSOUNDBANK_H_

#include <SDL/SDL_mixer.h>
#include <vector>

class CSoundBank {
    public:
        CSoundBank();

        static CSoundBank SoundControl;

        std::vector<Mix_Chunk*> SoundList;

        int OnLoad(char* File);

        void OnCleanup();

        void Play(int ID);

        void PlayLoop(int ID);
};

#endif







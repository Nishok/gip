#ifndef _CAPP_H_
    #define _CAPP_H_

#include <SDL/SDL.h>
#include "SDL/SDL_ttf.h"
#include <sstream>

#include "CSurface.h"
#include "CAnimation.h"
#include "CEvent.h"
#include "CEntity.h"
#include "Define.h"
#include "CArea.h"
#include "CCamera.h"
#include "CPlayer.h"
#include "COrc.h"
#include "CTTF.h"
#include "CProjectile.h"
#include "CSoundBank.h"
#include "CTimeEntity.h"

using namespace std;

class CApp : public CEvent {
    private:
        bool Running;

        Uint32 start;

        SDL_Surface *Surf_Display;  //Surf_Display is like a piece of paper where you can draw stuff onto

        CPlayer Player;
        COrc Orc;
        CTTF TTF;
        CProjectile Projectile;
        CTimeEntity TimeEntity;

    public:
        CApp();
        ~CApp(); //Make the destructor to stop memleaks

        int SoundsA, SoundsB, SoundsC;

        int OnExecute();

        float time;

        bool timerRunning;

        bool OnInit();  // Handles all the data loading (textures, maps, NPCs, etc)

        void OnEvent(SDL_Event* Event); // Handles input events such as mouse, keyboard, etc

        void OnExit(); // The call for exiting our program

        void OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode); // The function when a buton is held down

        void OnKeyUp(SDLKey sym, SDLMod mod, Uint16 unicode);   //The function when a button is released

        void OnLoop();  // Handles data updates (NPCs running around, decreasing HP, etc)

        void OnRender();    // Handles the rendering, anything which needs to be shown on the screen)

        void OnCleanup();   // Cleans up any resources loaded, to safely close the program

        void Reset();   // Handles the reset of the field

        CPlayer &AccuierePlayer();

        COrc &AccuiereOrc();

        CProjectile &AccuiereProjectile();

};

extern CApp *app;

#endif

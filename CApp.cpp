//==============================================================================
#include "CApp.h"

CApp *app = NULL;

//==============================================================================
CApp::~CApp() {     //Make sure everything gets set to NULL in here too, sometimes it may happen that it still leaks a value if not done.
    Surf_Display = NULL;
}

CApp::CApp() {
    Surf_Display = NULL;

    app = this;
    Running = true;
    timerRunning = true;
}

static Uint32 next_time;    //Unsigned Integer, so basically it can't go below 0
Uint32 time_left(void) {
    Uint32 now;

    now = SDL_GetTicks();
    if(next_time <= now)
        return 0;
    else
        return next_time - now;
}

//------------------------------------------------------------------------------
int CApp::OnExecute() {
    if(OnInit() == false) {
        cerr << "Error: Unable to initialize SDL video: " << SDL_GetError() << endl;
        return -1;
    }

    if (TTF_Init() == -1)
    {
        cerr << "TTF_Init() Failed: " << TTF_GetError() << endl;
        SDL_Quit();
    }

    start = SDL_GetTicks();

    SDL_Event Event;
    next_time = SDL_GetTicks() + (1000/FPS_MAX);

    while(Running) {
        if(timerRunning == true) {
            //time = "Timer: ";
            time = (SDL_GetTicks() - start) / 1000;     //The counter to count the time
        }

        SDL_Delay(time_left());
        next_time += (1000/FPS_MAX);

        while(SDL_PollEvent(&Event)) {
            OnEvent(&Event);
        }

        OnLoop();
        OnRender();
    }

    OnCleanup();

    return 0;
}

CPlayer &CApp::AccuierePlayer()
{
    return Player;
}

COrc &CApp::AccuiereOrc()
{
    return Orc;
}

CProjectile &CApp::AccuiereProjectile()
{
    return Projectile;
}

//==============================================================================
int main(int argc, char* argv[]) {
    CApp theApp;

    return theApp.OnExecute();
}

//==============================================================================

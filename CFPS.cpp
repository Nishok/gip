#include "CFPS.h"

CFPS CFPS::FPSControl;  //This is needed if you make a static variable in a class

CFPS::CFPS() {  //Starting with everything at 0, so we don't end up with errors
    OldTime = 0;
    LastTime = 0;
    SpeedFactor = 0;
    Frames = 0;
    MaxFrames = FPS_MAX;
    NumFrames = 0;
}

void CFPS::OnLoop() {
    if(OldTime + 1000 < (int)SDL_GetTicks()) {   //1000 = 1 sec (in milliseconds), we see if we've passed 1 second in ticks yet
        OldTime = SDL_GetTicks();   //Set OldTime to our current Tickrate

        NumFrames = Frames; //NumFrames is the current FPS of the game. Here we update the game's frames by changing it to 'Frames'

        Frames = 0; // Reset frames back to 0
    }

    SpeedFactor = ((SDL_GetTicks() - LastTime) / 1000.0f) * 32.0f;  //Calculate the speed we have to get for the computer's tickrate

    LastTime = SDL_GetTicks();  //Gets the computer's tickrate, so we can caluclate the speed we need

    Frames++;
}

int CFPS::GetFPS() {    // So we can get our current FPS rate
    return NumFrames;
}

float CFPS::GetSpeedFactor() {  //So we can return the speed we need for the computer to handle
    return SpeedFactor;
}

#include "CAnimation.h"

CAnimation::CAnimation() {
    CurrentFrame = 0;
    MaxFrames = 0;
    FrameInc = 1;

    FrameRate = 100; //In milliseconds
    OldTime = 0;

    Oscillate = false;
}

void CAnimation::OnAnimate() {
    if(OldTime + FrameRate > (int)SDL_GetTicks()) {  //If the Framerate gets higher than the the max ticks we can get per second, shutdown.
        return;
    }

    OldTime = SDL_GetTicks();

    CurrentFrame += FrameInc;

    if(Oscillate) {
        if(FrameInc > 0) {
            if(CurrentFrame >= MaxFrames - 1) {
                FrameInc = -FrameInc;
            }
        }
        else {
            if(CurrentFrame <= 0) {
                FrameInc = -FrameInc;
            }
        }
    }
    else {
        if(CurrentFrame >= MaxFrames - 1) { //If CurrentFrame hits the Max, set back to 0 and continue.
            CurrentFrame = 0;
        }
    }
}

void CAnimation::SetFrameRate(int Rate) {
    FrameRate = Rate;
}

void CAnimation::SetCurrentFrame(int Frame) {
    if(Frame < 0 || Frame >= MaxFrames) {   //If the Frame is 0 or if the Frame hits the maximum or higher, shutdown.
        return;
    }

    CurrentFrame = Frame;
}

int CAnimation::GetCurrentFrame() {
    return CurrentFrame;
}

#ifndef _CSURFACE_H_
    #define _CSURFACE_H_

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

class CSurface {
    public:
        CSurface();

        static SDL_Surface* OnLoad(char* File); //loads the surface for us

        static SDL_Surface* OnLoadAlpha(char* File);

        static bool OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int X, int Y); //(old surface to draw on, new surface to be drawn, X, Y)

        static bool OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int X, int Y, int X2, int Y2, int W, int H); //The function to copy a part from an image.

        static bool Transparent(SDL_Surface* Surf_Dest, int R, int G, int B); // This is so we can make colored backgrounds go transparant.

};

#endif

#include "CEntity.h"
#include "CProjectile.h"
#include "CSoundBank.h"
#include "CApp.h"

#include <iostream>

using namespace std;

std::vector<CEntity*> CEntity::EntityList;

CEntity::CEntity() {    //Pretty much set everything to 0 or NULL to initiate (start) with
    Surf_Entity = NULL;

    X = 0;
    Y = 0;

    Width = 0;
    Height = 0;

    MoveLeft = false;
    MoveRight = false;
    MoveUp = false;
    MoveDown = false;

    Type = ENTITY_TYPE_GENERIC;

    Dead = false;
    Flags = ENTITY_FLAG_GRAVITY;

    SpeedX = 0;
    SpeedY = 0;

    AccelX = 0;
    AccelY = 0;

    MaxSpeedX = WALKSPEED_MAX;
    MaxSpeedY = WALKSPEED_MAX;

    CurrentFrameCol = 0;
    CurrentFrameRow = 0;

    Col_X = 0;
    Col_Y = 0;

    Col_Width = 0;
    Col_Height = 0;
}

CEntity::~CEntity() {
}

bool CEntity::OnLoad(char* File, int Width, int Height, int MaxFrames) {
	if((Surf_Entity = CSurface::OnLoad(File)) == NULL) {
		return false;
	}

	CSurface::Transparent(Surf_Entity, 255, 0, 255);

	this->Width = Width;
	this->Height = Height;

	Anim_Control.MaxFrames = MaxFrames;

    return true;
}

void CEntity::OnLoop() {
    if(MoveLeft == false && MoveRight == false) {   //If we aren't running, we make it run StopMoveX()
        StopMoveX();
    }
    if(MoveUp == false && MoveDown == false) {   //If we aren't running, we make it run StopMoveY()
        StopMoveY();
    }

    if(MoveLeft) {     //Accelerate the entity when Left is triggered
        AccelX = -0.5;
    }
    if(MoveRight) {    //Accelerate the entity when Right is triggered
        AccelX = 0.5;
    }

    if(MoveUp) {     //Accelerate the entity when Up is triggered
        AccelY = -0.5;
    }
    if(MoveDown) {    //Accelerate the entity when Down is triggered
        AccelY = 0.5;
    }

    SpeedX += AccelX * CFPS::FPSControl.GetSpeedFactor();    //Set the speed acording to our Speedfactor
    SpeedY += AccelY * CFPS::FPSControl.GetSpeedFactor();

    if(SpeedX > MaxSpeedX)  SpeedX =  MaxSpeedX;	//Make sure our entity doesn't go faster than the maximum speed
	if(SpeedX < -MaxSpeedX) SpeedX = -MaxSpeedX;
	if(SpeedY > MaxSpeedY)  SpeedY =  MaxSpeedY;
	if(SpeedY < -MaxSpeedY) SpeedY = -MaxSpeedY;

    OnAnimate();    //Animate the entity
    OnMove(SpeedX, SpeedY);     //Move the entity
}

void CEntity::OnRender(SDL_Surface* Surf_Display) {

}

void CEntity::OnCleanup() {
    if(Surf_Entity) {
        SDL_FreeSurface(Surf_Entity);   //Free up the surface Surf_Entity if it exists
    }

    Surf_Entity = NULL;     //Set Surf_Entity to NULL so we don't end up drawing it again on accident

    Dead = true;
}

void CEntity::OnAnimate() {
    Anim_Control.OnAnimate();
}

bool CEntity::OnCollision(CEntity* Entity) {
        return true;
}

void CEntity::OnMove(float MoveX, float MoveY) {
    if(MoveX == 0 && MoveY == 0) return;    //If MoveX and Y are 0, then do nothing.

    double NewX = 0;
    double NewY = 0;

    MoveX *= CFPS::FPSControl.GetSpeedFactor();
    MoveY *= CFPS::FPSControl.GetSpeedFactor();

    if(MoveX != 0) {    //Calcumate how much closer we need to get to our desire point
        if(MoveX >= 0) NewX = CFPS::FPSControl.GetSpeedFactor();
        else NewX = -CFPS::FPSControl.GetSpeedFactor();
    }

    if(MoveY != 0) {
        if(MoveY >= 0) NewY = CFPS::FPSControl.GetSpeedFactor();
        else NewY = -CFPS::FPSControl.GetSpeedFactor();
    }

    while(true) {
        if(Flags & ENTITY_FLAG_GHOST || Flags & ENTITY_FLAG_PLYDETECT) {     //If entity is a ghost, then we still move even if a collision has happened
            PosValid((int)(X + NewX), (int)(Y + NewY)); //Check if the new position is a valid position, so other entities get notified that this will be the new position

            X += NewX;
            Y += NewY;
        }else {
            if(PosValid((int)(X + NewX), (int)(Y))) {   //Check if the new position will be valid
                X += NewX;  //Give him the new X-coords
            }
            else {
                SpeedX = 0;
            }

            if(PosValid((int)(X), (int)(Y + NewY))) {   //Check if the new position will be valid
                Y += NewY;  //Give him the new Y-coords
            }
            else {
                SpeedY = 0;
            }
        }

        MoveX += -NewX;
        MoveY += -NewY;

        if(NewX > 0 && MoveX <= 0) NewX = 0;         //Check if they reach 0, and then break the loop
        if(NewX < 0 && MoveX >= 0) NewX = 0;

        if(NewY > 0 && MoveY <= 0) NewY = 0;
        if(NewY < 0 && MoveY >= 0) NewY = 0;

        if(MoveX == 0) NewX = 0;
        if(MoveY == 0) NewY = 0;

        if(MoveX == 0 && MoveY == 0) break;
        if(NewX == 0 && NewY == 0) break;
    }
}

void CEntity::StopMoveX() {
    if(SpeedX > 0) {    //If going right, decrease the speed by -1 to slow down
        AccelX = -1;
    }

    if(SpeedX < 0) {    //If going left, decrease the speed by -1 to slow down
        AccelX = 1;
    }

    if(SpeedX < 2.0f && SpeedX > -2.0f) {   //Make it stop as soon as it reaches between 2 and -2 (so it doesn't loop back and forth)
        AccelX = 0;
        SpeedX = 0;
    }
}

void CEntity::StopMoveY() {
    if(SpeedY > 0) {    //If going up, decrease the speed by -1 to slow down
        AccelY = -1;
    }

    if(SpeedY < 0) {    //If going down, decrease the speed by -1 to slow down
        AccelY = 1;
    }

    if(SpeedY < 2.0f && SpeedY > -2.0f) {   //Make it stop as soon as it reaches between 2 and -2 (so it doesn't loop back and forth)
        AccelY = 0;
        SpeedY = 0;
    }
}

bool CEntity::Collides(int oX, int oY, int oW, int oH) {
    int left1, left2;   //Entity1 and Entity2
    int right1, right2;
    int top1, top2;
    int bottom1, bottom2;

    int tX = (int)X + Col_X;    //place at map + the size of where the entity's collision starts
    int tY = (int)Y + Col_Y;

    left1 = tX;
    left2 = oX;

    right1 = left1 + Width - 1 - Col_Width;     //We do -1 to get the true coordinate of the side
    right2 = oX + oW - 1;

    top1 = tY;
    top2 = oY;

    bottom1 = top1 + Height - 1 - Col_Height;
    bottom2 = oY + oH - 1;


    if (bottom1 < top2) return false;   //Checking if the entities' sides are away from eachother, if so then return false (false = no collision)
    if (top1 > bottom2) return false;

    if (right1 < left2) return false;
    if (left1 > right2) return false;

    return true;
}

bool CEntity::PosValid(int NewX, int NewY) {
    bool Return = true;

    int StartX     = (NewX + Col_X) / TILE_SIZE;    //Calculating our starter's TileID
    int StartY     = (NewY + Col_Y) / TILE_SIZE;

    int EndX    = ((NewX + Col_X) + Width - Col_Width)         / TILE_SIZE;     //Same, but instead we get the area of tiles where the entity's at
    int EndY    = ((NewY + Col_Y) + Height - Col_Height)    / TILE_SIZE;

    for(int iY = StartY;iY <= EndY;iY++) {  //Checking every tile we pass through
        for(int iX = StartX;iX <= EndX;iX++) {
            CTile* Tile = CArea::AreaControl.GetTile(iX * TILE_SIZE, iY * TILE_SIZE);

            if(PosValidTile(Tile) == false) {   //If the tile is not valid, then we change Return to false
                Return = false;
            }
        }
    }

    if(Flags & ENTITY_FLAG_MAPONLY) {   //Check if the flag is enabled, if so, then we don't have to go further since there will be no entity collision
    }
    else if(Flags ^ ENTITY_FLAG_PLYDETECT) {
        for(int i = 0;i < (int)EntityList.size();i++) {  //We go through the entitylist and pass each entity to PosValidEntity
            if(PosValidEntity(EntityList[i], NewX, NewY) == false) {
                Return = true;
            }
        }
    }
    else {
        for(int i = 0;i < (int)EntityList.size();i++) {  //We go through the entitylist and pass each entity to PosValidEntity
            if(PosValidEntity(EntityList[i], NewX, NewY) == false) {
                Return = false;
            }
        }
    }

    return Return;
}

bool CEntity::PosValidTile(CTile* Tile) {
    if(Tile == NULL) {  //If the tile is NULL, then return true so we can walk further
        return true;
    }

    if(Tile->TypeID == TILE_TYPE_BLOCK) {   //If the tile is a BLOCK type, then no we cannot pass through
        return false;
    }

    return true;    //Yes, make every other tile passable
}

bool CEntity::PosValidEntity(CEntity* Entity, int NewX, int NewY) {
    if(this != Entity && Entity != NULL && Entity->Dead == false && //check if all this stuff is true or not
        Entity->Flags ^ ENTITY_FLAG_MAPONLY && Entity->Type ^ ENTITY_TYPE_ENEMY &&
        Entity->Collides(NewX + Col_X, NewY + Col_Y, Width - Col_Width, Height - Col_Height) == true) {

        CEntityCol EntityCol;

        EntityCol.EntityA = this;   //EntityA will be the controlled entity
        EntityCol.EntityB = Entity; //EntityB will be the entity we collide into

        CEntityCol::EntityColList.push_back(EntityCol);

        return false;   //Position is invalid
    }

    return true;    //If no entity infront, then there will be no block
}

bool CEntity::Fire() {
            CProjectile *Projectile = new CProjectile();    //Create a new projectile
            *Projectile = app->AccuiereProjectile();    //Grab the new projectile that we just spawned, so we can put details in it
            CEntity::EntityList.push_back(Projectile);  //Put the new projectile in the entitylist

            if(this->Direction == 'R') {    //Get the player's direction
                Projectile->MoveLeft = false; Projectile->MoveDown = false; Projectile->MoveUp = false;     //Stop the projectile from moving, otherwise it'll move into all directions.
                Projectile->SpeedY = 0;     //Set the speed of the projectile to 0.
                Projectile->X = (this->X + 35);  //Get the current player's position and add 35 to X, so the projectile doesn't get stuck inside of him
                Projectile->Y = (this->Y + 10);  //Same for the Y-axis
                Projectile->SpeedX = Projectile->MaxSpeedX;   //Set the projectile's speed to the maximum speed
                Projectile->MoveRight = true;    //Launch the projectile right
                cerr << "Test" << endl;
            }

            if(this->Direction == 'L') {
                Projectile->MoveRight = false; Projectile->MoveDown = false; Projectile->MoveUp = false;
                Projectile->SpeedY = 0;
                int PlyX = this->X;
                int PlyY = this->Y;
                Projectile->X = (PlyX - 20);
                Projectile->Y = (PlyY + 10);
                Projectile->SpeedX = -Projectile->MaxSpeedX;
                Projectile->MoveLeft = true;
            }

            if(this->Direction == 'U') {
                Projectile->MoveDown = false; Projectile->MoveRight = false; Projectile->MoveLeft = false;
                Projectile->SpeedX = 0;
                int PlyX = this->X;
                int PlyY = this->Y;
                Projectile->X = (PlyX + 8);
                Projectile->Y = (PlyY - 20);
                Projectile->SpeedY = -Projectile->MaxSpeedY;
                Projectile->MoveUp = true;
            }

            if(this->Direction == 'D') {
                Projectile->MoveUp = false; Projectile->MoveRight = false; Projectile->MoveLeft = false;
                Projectile->SpeedX = 0;
                int PlyX = this->X;
                int PlyY = this->Y;
                Projectile->X = (PlyX + 8);
                Projectile->Y = (PlyY + 40);
                Projectile->SpeedY = Projectile->MaxSpeedY;
                Projectile->MoveDown = true;
            }
    return true;
}

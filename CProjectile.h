#ifndef _CPROJECTILE_H_
    #define _CPROJECTILE_H_

#include <iostream>

#include "CEntity.h"

using namespace std;

class CProjectile : public CEntity {
    public:
        CProjectile();

        //static SDL_Surface* Surf_Projectile;

		bool OnLoad(char* File, int Width, int Height, int MaxFrames);

        void OnLoop();

        void OnRender(SDL_Surface* Surf_Display);

        void OnCleanup();

        void OnAnimate();

        bool OnCollision(CEntity* Entity);

        bool PosValidTile(CTile* Tile);

        //bool Fire();

        //CProjectile &operator=(CProjectile rhs);
};

#endif

#ifndef _CENTITY_H_
    #define _CENTITY_H_

#include <vector>
#include <iostream>

#include "CArea.h"
#include "CAnimation.h"
#include "CCamera.h"
#include "CFPS.h"
#include "CSurface.h"
#include "CTTF.h"
#include "Define.h"


enum {
    ENTITY_TYPE_GENERIC = 0,
    ENTITY_TYPE_PLAYER,
    ENTITY_TYPE_ENEMY,
    ENTITY_TYPE_PROJECTILE,
    ENTITY_TYPE_ENDPLATFORM
};

enum {
    ENTITY_FLAG_NONE = 0,
    ENTITY_FLAG_GRAVITY = 0x00000001,   //Entities affected by gravity
    ENTITY_FLAG_GHOST = 0x00000002,     //entities that can go through walls
    ENTITY_FLAG_MAPONLY = 0x00000004,    //Entities that only collides with the map, and goes through other entities
    ENTITY_FLAG_PLYDETECT = 0x0000008
    //The reason why they are numbered in hex is so we can trigger more flags at once
};

class CEntity {
    protected:
        CAnimation Anim_Control;

        SDL_Surface* Surf_Entity;

        float SpeedX;   //The current speed of the entity
        float SpeedY;
        float AccelX;   //The acceleration speed
        float AccelY;

        int CurrentFrameCol;    //To access the entity's column in the tileset
        int CurrentFrameRow;
        int Col_X;  //These variables are to determine the collision so it doesn't collide on the image's size, but on the character's body.
        int Col_Y;  //so when you hit a wall, you wouldn't be standing a few pixels back
        int Col_Width;
        int Col_Height;

        //bool CanJump;

    public:
        bool PosValid(int NewX, int NewY);  //This checks if the entity is trying to go to make a valid move
        bool PosValidTile(CTile* Tile);
        bool PosValidEntity(CEntity* Entity, int NewX, int NewY);

    public:
        static std::vector<CEntity*> EntityList;

        CEntity();

        virtual ~CEntity();

        virtual bool OnLoad(char* File, int Width, int Height, int MaxFrames);  //Virtual means that you can change this in any other file, so you can have different values for each file.
        virtual void OnLoop();
        virtual void OnRender(SDL_Surface* Surf_Display);
        virtual void OnCleanup();
        virtual void OnAnimate();   //To define animation state, left or right animations etc
        virtual bool OnCollision(CEntity* Entity);

        float X;
        float Y;
        float MaxSpeedX;    //The maximum speed you can reach, this is so you won't keep accelerating until infinite when walking.
        float MaxSpeedY;
        float LifeTime;
        float FireDelay;
        float Interval;
        float Interval1;
        float Interval2;
        float HPInterval;
        float AttackDelay;
        float WalkInterval;
        float HeartBeatDelay;
        float HealthlossDelay;

        int Width;
        int Height;
        int Type;
        int Flags;
        int Health;
        int Kills;
        int Deaths;

        char Direction;

        bool MoveLeft;
        bool MoveRight;
        bool MoveUp;
        bool MoveDown;
        bool Dead;
        bool Launched;
        bool Collides(int oX, int oY, int oW, int oH);
        bool Fire();

        void OnMove(float MoveX, float MoveY);
        void StopMoveX();    //This will trigger the acceleration going negative, so we start breaking, and not suddenly stop (for the Left & Right side)
        void StopMoveY();    //This will trigger the acceleration going negative, so we start breaking, and not suddenly stop (For the Up and Down side)
};

class CEntityCol {
    public:
        static std::vector<CEntityCol> EntityColList;

        CEntity* EntityA;
        CEntity* EntityB;

        CEntityCol();
};

#endif

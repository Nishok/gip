#ifndef _CMAP_H_
    #define _CMAP_H_

#include <SDL/SDL.h>
#include <iostream>
#include <vector>

#include "CTile.h"
#include "CSurface.h"

class CMap {
    private:
        std::vector<CTile> TileList;

    public:
        SDL_Surface* Surf_Tileset;

        CMap();

        bool OnLoad(char* File);    //To load the maps from the files

        void OnRender(SDL_Surface* Surf_Display, int MapX, int MapY);    //to draw the maps loaded from the files

        CTile* GetTile(int X, int Y);
};

#endif

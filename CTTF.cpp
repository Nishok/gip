#include "CTTF.h"
#include "CApp.h"

CTTF CTTF::TTF;

CTTF::~CTTF(){     //Make sure everything gets set to NULL in here too, sometimes it may happen that it still leaks a value if not done.
    Surf_HealthMessage = NULL;
    Surf_TimeMessage = NULL;
    Surf_KillsMessage = NULL;
    Surf_PosMessage = NULL;
    fontBig = NULL;
    fontMedium = NULL;
    fontSmall = NULL;
}

CTTF::CTTF() {
    Surf_HealthMessage = NULL;
    Surf_TimeMessage = NULL;
    Surf_KillsMessage = NULL;
    Surf_PosMessage = NULL;
    fontBig = NULL;
    fontMedium = NULL;
    fontSmall = NULL;

    if(!TTF_WasInit())
        TTF_Init();

    // Load a font
    fontBig = TTF_OpenFont("FreeSans.ttf", 24);
    fontMedium = TTF_OpenFont("FreeSans.ttf", 20);
    fontSmall = TTF_OpenFont("FreeSans.ttf", 16);

    if (fontBig == NULL || fontMedium == NULL || fontSmall == NULL)
    {
        cerr << "TTF_OpenFont() Failed: " << TTF_GetError() << endl;
        TTF_Quit();
        SDL_Quit();
    }
}

void CTTF::OnRender(SDL_Surface* Surf_Display) {
    //cerr << "Note: Loading TTF OnRender" << endl;

    int HP = app->AccuierePlayer().Health;  //Get the health of the player
    char cHP[5];
    sprintf(cHP, "%i", HP);     //Convert the integer of the health into a *char
    char buffer[MAX_PATH];  //The char where both Health: and the health number will be stored in
    strcpy(buffer, "Health: ");     //Copy Health: into buffer
    strcat(buffer, cHP);    //Put the cHP char behind what is already stored into the buffer

    int Timer = app->time;
    char cTimer[15];
    sprintf(cTimer, "%i", Timer);
    char buffer2[MAX_PATH];
    strcpy(buffer2, "Time: ");
    strcat(buffer2, cTimer);

    int Kills = app->AccuierePlayer().Kills;
    char cKills[5];
    sprintf(cKills, "%i", Kills);
    char buffer3[MAX_PATH];
    strcpy(buffer3, "Kills: ");
    strcat(buffer3, cKills);

    int PlyX = app->AccuierePlayer().X;
    int PlyY = app->AccuierePlayer().Y;
    char cPosX[15];
    char cPosY[15];
    sprintf(cPosX, "%i", PlyX);
    sprintf(cPosY, "%i", PlyY);
    char buffer4[MAX_PATH];
    strcpy(buffer4, "Pos: ");
    strcat(buffer4, cPosX);
    strcat(buffer4, ", ");
    strcat(buffer4, cPosY);

    SDL_Color Red = {255, 0, 0};
    //SDL_Color Green = {0, 255, 0};
    //SDL_Color Blue = {0, 0, 255};
    //SDL_Color Black = {0, 0, 0};
    SDL_Color White = {255, 255, 255};

    TTF_SetFontStyle(fontBig, TTF_STYLE_BOLD);    //Gives the font a special property (bold, italic, underline, crossthrough, normal)
    //TTF_SetFontOutline(fontBig, 0.2);     //Draws a border on the text

    Surf_HealthMessage = TTF_RenderText_Blended(fontBig, buffer, Red);     //Solid = normal text, Shaded = box behind text, Blended = Anti-aliased
    Surf_TimeMessage = TTF_RenderText_Blended(fontBig, buffer2, White);
    Surf_KillsMessage = TTF_RenderText_Blended(fontSmall, buffer3, White);
    Surf_PosMessage = TTF_RenderText_Blended(fontBig, buffer4, White);

    if (Surf_HealthMessage == NULL || Surf_TimeMessage == NULL || Surf_KillsMessage == NULL || Surf_PosMessage == NULL || Surf_Display == NULL)
    {
        cerr << "TTF_RenderText_Solid() Failed: " << TTF_GetError() << endl;
        TTF_Quit();
        SDL_Quit();
    }

    CSurface::OnDraw(Surf_Display, Surf_HealthMessage, 5, 5);   //Draw Surf_HealthMessage ontop of the Surf_Display
    CSurface::OnDraw(Surf_Display, Surf_TimeMessage, (WWIDTH - Surf_TimeMessage->w) / 2, 5);
    CSurface::OnDraw(Surf_Display, Surf_KillsMessage, 5, (Surf_HealthMessage->h));
    CSurface::OnDraw(Surf_Display, Surf_PosMessage, (WWIDTH - Surf_PosMessage->w - 5), (WHEIGHT - Surf_KillsMessage->h - 5));

    SDL_FreeSurface(Surf_HealthMessage);  //This needs to be done because SDL_TTF keeps on drawing the text forever, so we'll also need to keep on cleaning it forever or else memory leaks will happen.
    SDL_FreeSurface(Surf_TimeMessage);
    SDL_FreeSurface(Surf_KillsMessage);
    SDL_FreeSurface(Surf_PosMessage);
}

void CTTF::OnCleanup() {
    if(Surf_HealthMessage) {
        SDL_FreeSurface(Surf_HealthMessage);   //Free up the surface Surf_HealthMessage if it exists
    }
    else if(Surf_TimeMessage) {
        SDL_FreeSurface(Surf_TimeMessage);
    }
    else if(Surf_KillsMessage) {
        SDL_FreeSurface(Surf_KillsMessage);
    }
    else if(Surf_PosMessage) {
        SDL_FreeSurface(Surf_PosMessage);
    }
    cerr << "Note: TTF Deloaded" << endl;

    TTF_Quit();

    Surf_HealthMessage = NULL;     //Set Surf_HealthMessage to NULL so we don't end up drawing it again on accident
    Surf_TimeMessage = NULL;
    Surf_KillsMessage = NULL;
    Surf_PosMessage = NULL;
}

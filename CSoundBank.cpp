#include "CSoundBank.h"

CSoundBank CSoundBank::SoundControl;

CSoundBank::CSoundBank() {
}

int CSoundBank::OnLoad(char* File) {
    Mix_Chunk* TempSound = NULL;    //Make sure TempSound is empty

    if((TempSound = Mix_LoadWAV(File)) == NULL) {   //Initialiwe Mix_LoadWAV and see if we have a sound file to load or not
        return -1;
    }

    SoundList.push_back(TempSound);     //Put the soundfile into the vector

    return (SoundList.size() -1);   //Return that soundfile we just inserted
}

void CSoundBank::OnCleanup() {
    for(int i = 0; i < (int)SoundList.size(); i++) {     //Go through the loop qnd free up every sound we have loaded
        Mix_FreeChunk(SoundList[i]);
    }

    SoundList.clear();  //Clean the while vector
}

void CSoundBank::Play(int ID) {
    if(ID < 0 || ID >= (int)SoundList.size()) return;
    if(SoundList[ID] == NULL) return;

    Mix_PlayChannel(-1, SoundList[ID], 0);  //First parameter is the channel, I choose -1 so SDL_mixer chooses a good chqnnel automqtically
                                            //The second is the sound from the SoundList
                                            //Third is the loop. Now we only play once, -1 makes it infinite and for example 5 will make it play 6 times in total
}

void CSoundBank::PlayLoop(int ID) {
    if(ID < 0 || ID >= (int)SoundList.size()) return;
    if(SoundList[ID] == NULL) return;

    Mix_PlayChannel(-1, SoundList[ID], -1);  //First parameter is the channel, I choose -1 so SDL_mixer chooses a good chqnnel automqtically
                                            //The second is the sound from the SoundList
                                            //Third is the loop. Now we only play once, -1 makes it infinite and for example 5 will make it play 6 times in total
}

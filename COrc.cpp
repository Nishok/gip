#include "COrc.h"
#include "CApp.h"

COrc::COrc() {
    X = 0;
    Y = 0;

    Width = 0;
    Height = 0;

    MoveLeft = false;
    MoveRight = false;
    MoveUp = false;
    MoveDown = false;

    CurrentFrameCol = 3;
    CurrentFrameRow = 0;

    MaxSpeedX = NPC_FAST;
    MaxSpeedY = NPC_FAST;

    Health = 2;
    Interval = 0;
    AttackDelay = 2;

    Type = ENTITY_TYPE_ENEMY;
}

bool COrc::OnLoad(char* File, int Width, int Height, int MaxFrames) {
    if(CEntity::OnLoad(File, Width, Height, MaxFrames) == false) {
        return false;
    }

    return true;
}

void COrc::OnLoop() {
    CEntity::OnLoop();

    if((((X + 200) > app->AccuierePlayer().X) && ((X - 200) < app->AccuierePlayer().X)) && (((Y + 200) > app->AccuierePlayer().Y) && ((Y - 200) < app->AccuierePlayer().Y))) {
        if(((X) > app->AccuierePlayer().X)) {
            MoveLeft = true;
        }else {
            MoveLeft = false;
        }

        if((X) < app->AccuierePlayer().X) {
            MoveRight = true;
        }else {
            MoveRight = false;
        }

        if((Y) < app->AccuierePlayer().Y) {
            MoveDown = true;
        }else {
            MoveDown = false;
        }

        if((Y) > app->AccuierePlayer().Y) {
            MoveUp = true;
        }else {
            MoveUp = false;
        }

    }else {
        MoveDown = false;
        MoveLeft = false;
        MoveRight = false;
        MoveUp = false;
    }

    Interval += CFPS::FPSControl.GetSpeedFactor();

    if(Health <= 0) {
        Dead = true;
    }
}

void COrc::OnRender(SDL_Surface* Surf_Display) {
    CEntity::OnRender(Surf_Display);
    if(Surf_Entity == NULL || Surf_Display == NULL) return;     //If we don't have anything to draw, then we skip this function

    CSurface::OnDraw(Surf_Display, Surf_Entity, X - CCamera::CameraControl.GetX(), Y - CCamera::CameraControl.GetY(), (CurrentFrameCol + Anim_Control.GetCurrentFrame())  * Width, (CurrentFrameRow) * Height, Width, Height);
}

void COrc::OnCleanup() {
    CEntity::OnCleanup();
}

void COrc::OnAnimate() {
    if(SpeedX != 0 || SpeedY != 0) {
        Anim_Control.MaxFrames = 3;     //If our speed is higher than 0, then the maxframes will be 3 (so you'll get the animation).
    }else{
        Anim_Control.MaxFrames = 0;     //If your speed is 0, then there will be no animation.
    }

    if(MoveLeft) {
        CurrentFrameRow = 3;    //Makes the animation look left
    }
    else if(MoveRight) {
        CurrentFrameRow = 2;     //Makes the animation look right
    }
    else if(MoveUp) {
        CurrentFrameRow = 1;     //Makes the animation look up
    }
    else if(MoveDown) {
        CurrentFrameRow = 0;     //Makes the animation look down
    }

    Anim_Control.OnAnimate();
}

bool COrc::OnCollision(CEntity* Entity) {
    if(Entity->Type == ENTITY_TYPE_PLAYER) {
        Dead = true;
    }

    if(Entity->Type == ENTITY_TYPE_PROJECTILE) {
        Dead = true;
        if(Interval > AttackDelay) {
            app->AccuierePlayer().Kills += 1;
            Interval = 0;
        }
    }

    return true;
}

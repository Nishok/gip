#include "CApp.h"

void CApp::OnEvent(SDL_Event* Event) {
    CEvent::OnEvent(Event);
}

void CApp::OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode) {

    switch(sym) {
        case SDLK_F10: OnExit(); break;

        case SDLK_LEFT: {
            if(Player.MoveRight == true) {
                break;
            }
            Player.MoveLeft = true;
            break;
        }

        case SDLK_t: {
                Player.Flags = ENTITY_FLAG_GHOST;
                break;
        }

        case SDLK_p: {
                Player.Health += 1;
                break;
        }

        case SDLK_m: {
                Player.Health -= 1;
                break;
        }

        case SDLK_r: {
            Player.Flags = ENTITY_FLAG_GRAVITY;
            Player.Health = 5;
            break;
        }

        case SDLK_RIGHT: {
            if(Player.MoveLeft == true) {
                break;
            }
            Player.MoveRight = true;
            break;
        }

        case SDLK_UP: {
            if(Player.MoveDown == true) {
                break;
            }
            Player.MoveUp = true;
            break;
        }

        case SDLK_DOWN: {
            if(Player.MoveUp == true) {
                break;
            }
            Player.MoveDown = true;
            break;
        }

        case SDLK_SPACE: {
            Player.Fire();
            break;
        }

        case SDLK_s: {
            if(timerRunning == true) {
                timerRunning = false;
                start = 0;
            } else {
                timerRunning = true;
                start = SDL_GetTicks();
            }
            break;
        }

        case SDLK_RETURN: {
            char area[20] = "./maps/1.area";
            CArea::AreaControl.OnLoad(area);
            break;
        }

        default: break;
    }
}

void CApp::OnKeyUp(SDLKey sym, SDLMod mod, Uint16 unicode) {
    switch(sym) {
        case SDLK_LEFT: {
            Player.MoveLeft = false;
            break;
        }

        case SDLK_RIGHT: {
            Player.MoveRight = false;
            break;
        }

        case SDLK_UP: {
            Player.MoveUp = false;
            break;
        }

        case SDLK_DOWN: {
            Player.MoveDown = false;
            break;
        }

        case SDLK_SPACE: {
        }

        default: {
        }
    }
}

void CApp::OnExit() {   //Define what OnExit() will do when called
    Running = false;
}

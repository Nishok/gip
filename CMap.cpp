#include "CMap.h"

CMap::CMap() {
    Surf_Tileset = NULL;    //Making sure the tileset has no value, otherwise it could cause problems
}

bool CMap::OnLoad(char* File) {
    TileList.clear();   //Make sure it's cleared before using it again

    FILE* FileHandle = fopen(File, "r");    //Open a file to read (r = read)

    if(FileHandle == NULL) {    //If the file doesn't exist, stop
        return false;
    }

    for(int Y = 0; Y < MAP_HEIGHT; Y++) {   //Prevents from reading more tiles than our map's height
        for(int X = 0; X < MAP_WIDTH; X++) {    //Prevents from reading more tiles than our map's width
            CTile tempTile;
            char TileID;

            //fscanf(FileHandle, "%d:%d:%d ", &tempTile.TileIDX, &tempTile.TileIDY, &tempTile.TypeID);   //Scan through the file and save the %d into Tile and Type, %d means Decimal integer.
            fscanf(FileHandle, "%c", &TileID);

            switch (TileID) {
                case 'a':
                    tempTile.TileIDX = 3;
                    tempTile.TileIDY = 8;
                    tempTile.TypeID = 2;
                    break;

                case 'z':
                    tempTile.TileIDX = 4;
                    tempTile.TileIDY = 8;
                    tempTile.TypeID = 2;
                    break;

                case 'e':
                    tempTile.TileIDX = 5;
                    tempTile.TileIDY = 8;
                    tempTile.TypeID = 2;
                    break;

                case 'r':
                    tempTile.TileIDX = 6;
                    tempTile.TileIDY = 8;
                    tempTile.TypeID = 2;
                    break;

                case 't':
                    tempTile.TileIDX = 7;
                    tempTile.TileIDY = 8;
                    tempTile.TypeID = 2;
                    break;

                case 'y':
                    tempTile.TileIDX = 8;
                    tempTile.TileIDY = 8;
                    tempTile.TypeID = 2;
                    break;

                case 'u':
                    tempTile.TileIDX = 9;
                    tempTile.TileIDY = 8;
                    tempTile.TypeID = 2;
                    break;
                // Next
                case 'q':
                    tempTile.TileIDX = 3;
                    tempTile.TileIDY = 9;
                    tempTile.TypeID = 2;
                    break;

                case 's':
                    tempTile.TileIDX = 4;
                    tempTile.TileIDY = 9;
                    tempTile.TypeID = 2;
                    break;

                case 'd':
                    tempTile.TileIDX = 5;
                    tempTile.TileIDY = 9;
                    tempTile.TypeID = 2;
                    break;

                case 'f':
                    tempTile.TileIDX = 6;
                    tempTile.TileIDY = 9;
                    tempTile.TypeID = 2;
                    break;

                case 'g':
                    tempTile.TileIDX = 7;
                    tempTile.TileIDY = 9;
                    tempTile.TypeID = 2;
                    break;

                case 'h':
                    tempTile.TileIDX = 8;
                    tempTile.TileIDY = 9;
                    tempTile.TypeID = 2;
                    break;

                case 'j':
                    tempTile.TileIDX = 9;
                    tempTile.TileIDY = 9;
                    tempTile.TypeID = 2;
                    break;

                case 'k':
                    tempTile.TileIDX = 2;
                    tempTile.TileIDY = 8;
                    tempTile.TypeID = 2;
                    break;
                //Next
                case 'w':
                    tempTile.TileIDX = 4;
                    tempTile.TileIDY = 0;
                    tempTile.TypeID = 2;
                    break;
                //Next
                case 'x':
                    tempTile.TileIDX = 6;
                    tempTile.TileIDY = 2;
                    tempTile.TypeID = 2;
                    break;

                case 'c':
                    tempTile.TileIDX = 7;
                    tempTile.TileIDY = 2;
                    tempTile.TypeID = 2;
                    break;

                case 'v':
                    tempTile.TileIDX = 6;
                    tempTile.TileIDY = 3;
                    tempTile.TypeID = 2;
                    break;

                case 'b':
                    tempTile.TileIDX = 7;
                    tempTile.TileIDY = 3;
                    tempTile.TypeID = 2;
                    break;
                //next
                case 'o':
                    tempTile.TileIDX = 6;
                    tempTile.TileIDY = 6;
                    tempTile.TypeID = 2;
                    break;

                case 'p':
                    tempTile.TileIDX = 4;
                    tempTile.TileIDY = 2;
                    tempTile.TypeID = 2;
                    break;

                case 'l':
                    tempTile.TileIDX = 6;
                    tempTile.TileIDY = 7;
                    tempTile.TypeID = 2;
                    break;

                case 'm':
                    tempTile.TileIDX = 5;
                    tempTile.TileIDY = 2;
                    tempTile.TypeID = 2;
                    break;
                //next
                case 'n':
                    tempTile.TileIDX = 8;
                    tempTile.TileIDY = 0;
                    tempTile.TypeID = 1;
                    break;


                default:
                    break;
            }

            TileList.push_back(tempTile);   //Increase the vector size by one, othewise we'll be getting errors
        }
        fscanf(FileHandle, "\n");   //Go to the next row.
    }

    fclose(FileHandle); //Stop reading the file

    return true;
}

void CMap::OnRender(SDL_Surface* Surf_Display, int MapX, int MapY) {
    if(Surf_Tileset == NULL) {  //Check if we have a valid Tileset, if not then return. otherwise we crash
        return;
    }

    int ID = 0;

    for(int Y = 0; Y < MAP_HEIGHT; Y++) {	//Prevents us from drawing more than our map's size
        for(int X = 0; X < MAP_WIDTH; X++) {
            if(TileList[ID].TypeID == TILE_TYPE_NONE) { //If the tile ID is nothing, then we don't need to draw
                ID++;
                continue;
            }

            int tX = MapX + (X * TILE_SIZE);    //Doing this so we get the pixel coordinates, otherwise we'd only have the tile sizes
            int tY = MapY + (Y * TILE_SIZE);

            int TilesetX = TileList[ID].TileIDX * TILE_SIZE;
            int TilesetY = TileList[ID].TileIDY * TILE_SIZE;

            CSurface::OnDraw(Surf_Display, Surf_Tileset, tX, tY, TilesetX, TilesetY, TILE_SIZE, TILE_SIZE);

            ID++;
        }
    }
}

CTile* CMap::GetTile(int X, int Y) {
    int ID = 0;

    ID = X / TILE_SIZE;     //Get the ID of the maptile
    ID = ID + (MAP_WIDTH * (Y / TILE_SIZE));

    if(ID < 0 || ID >= (int)TileList.size()) {
        return NULL;
    }

    return &TileList[ID];
}

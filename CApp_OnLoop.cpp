#include "CApp.h"

#include <iostream>
void CApp::OnLoop() {
    for(int i = 0;i < (int)CEntity::EntityList.size();i++) {
        if(!CEntity::EntityList[i]) continue;

        CEntity::EntityList[i]->OnLoop();
    }

    for(int i = 0;i < (int)CEntityCol::EntityColList.size();i++) {   //Go through the list of collisions that have happened and are saved.
        CEntity* EntityA = CEntityCol::EntityColList[i].EntityA;    //Assign collisions to EntityA+B through a pointer.
        CEntity* EntityB = CEntityCol::EntityColList[i].EntityB;

        if(EntityA == NULL || EntityB == NULL) continue;    //Continue (skip the current loop number) if there is one of the entities missing.

        if(EntityA->OnCollision(EntityB)) {     //If the EntityA is on collision with EntityB, then also make EntityB notify he's colliding with EntityA.
            EntityB->OnCollision(EntityA);
        }
    }

    for(std::vector<CEntity*>::iterator it = CEntity::EntityList.begin(); it != CEntity::EntityList.end();) {   //Create an iterator to use to check the entitylist
        if(!(*it)) continue;    //See if the iterator of the current entitylist's position exists
        if((*it)->Dead == true) {   //If the entity is dead
                cerr << "IsDead" << endl;
                it = CEntity::EntityList.erase(it); //Remove the entity
                cerr << "IsRemoved" << endl;
        }
        else {
                it++;   //No dead entity? Go to the next position
        }
    }

    CEntityCol::EntityColList.clear();  //Clear the entitycollisionlist because we don't need it anymore since we've already used it in the loop above.

    CFPS::FPSControl.OnLoop();

    char Buffer[255];
    sprintf(Buffer, "Bjorn Truyts G.I.P.  -  FPS:%d", (CFPS::FPSControl.GetFPS() - 3));   //%d stands for decimal interger. OLD: CFPS::FPSControl.GetFPS() OR FPS_MAX
    SDL_WM_SetCaption(Buffer, Buffer);
}

#ifndef _CTTF_H_
    #define _CTTF_H_

#include <SDL/SDL.h>
#include <iostream>

#include "CSurface.h"

using namespace std;

class CTTF {
    private:
        SDL_Surface* Surf_HealthMessage;
        SDL_Surface* Surf_TimeMessage;
        SDL_Surface* Surf_KillsMessage;
        SDL_Surface* Surf_PosMessage;

    public:

        static CTTF TTF;

        CTTF();
        ~CTTF();     //Make the destructor to stop memleaks

        TTF_Font* fontBig;
        TTF_Font* fontMedium;
        TTF_Font* fontSmall;

        void OnRender(SDL_Surface* Surf_Display);
        void OnCleanup();
};

#endif

#include "CApp.h"
#include <windows.h>
#include <vector>

bool CApp::OnInit() {
    //freopen("CON", "w", stdout); // redirects stdout towards the console
    //freopen("CON", "w", stderr); // redirects stderr towards the console
    if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        return false;
    }

    if((Surf_Display = SDL_SetVideoMode(WWIDTH, WHEIGHT, 32, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL) {    //Width, height, bit resolution and display flags
        return false;
    }

    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) < 0) {     //Initialize the sound device. (frequency, bit format, channels(1 = mono and 2 is stereo), size ofn the sample)
        return false;
    }

    //char area[20] = "./maps/final.area";
    char area[20] = "./maps/1.area";
    if(CArea::AreaControl.OnLoad(area) == false) {
        return false;
    }

    char character[20] = "characters.png";
    if(Player.OnLoad(character, 32, 36, 3) == false) {
        return false;
    }

    //for(unsigned int i = 0; i < sizeof(Orc)/sizeof(Orc[0]); ++i) {
    //    if(Orc[i].OnLoad(character, 32, 36, 3) == false) {
    //        return false;
    //    }
    //}

    if(Orc.OnLoad(character, 32, 36, 3) == false) {
        return false;
    }


    char test[25] = "./shuriken.png";
    if(Projectile.OnLoad(test, 16, 16, 1) == false) {
        return false;
    }

    char soundFile[25] = "shiruken throw.wav";
    if((SoundsA = CSoundBank::SoundControl.OnLoad(soundFile)) == -1) {
        return false;
    }

    char soundFile1[25] = "heartbeat.wav";
    if((SoundsB = CSoundBank::SoundControl.OnLoad(soundFile1)) == -1) {
        return false;
    }

    char soundFile2[25] = "steps.wav";
    if((SoundsC = CSoundBank::SoundControl.OnLoad(soundFile2)) == -1) {
        return false;
    }

    char character1[20] = "characters1.png";
    if(TimeEntity.OnLoad(character1, 576, 32, 1) == false) {
        return false;
    }

    Player.X = PlayerSpawnX;
    Player.Y = PlayerSpawnY;

    TimeEntity.X = 4095;
    TimeEntity.Y = 545;

    int x, y;
    srand((unsigned int)timeGetTime());
    for(unsigned int i = 0; i < ORC_AMOUNT; ++i) {
        COrc *Orc = new COrc();    //Create a new orc
        *Orc = app->AccuiereOrc();    //Grab the new orc that we just spawned, so we can put details in it
        CEntity::EntityList.push_back(Orc);  //Put the new orc into the entitylist
        if(Orc->PosValid(x=rand()%7300 + 430, y=rand()%4900 + 550)) {
            Orc->X = x;
            Orc->Y = y;
        }
    }

    CEntity::EntityList.push_back(&TimeEntity);
    CEntity::EntityList.push_back(&Player);

    CCamera::CameraControl.TargetMode = TARGET_MODE_CENTER;
    CCamera::CameraControl.SetTarget(&Player.X, &Player.Y);

    SDL_EnableKeyRepeat(1, SDL_DEFAULT_REPEAT_INTERVAL / 3);

    return true;
}

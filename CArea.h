#ifndef _CAREA_H_
    #define _CAREA_H_

#include "CMap.h"

class CArea {
    private:
        int AreaSize;

        SDL_Surface* Surf_Tileset;

    public:
        static CArea AreaControl;

        std::vector<CMap> MapList;

        CArea();

        bool OnLoad(char* File);

        void OnRender(SDL_Surface* Surf_Display, int CameraX, int CameraY);

        CMap* GetMap(int X, int Y);

        CTile* GetTile(int X, int Y);

        void OnCleanup();
};

#endif

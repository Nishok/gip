#include "CSurface.h"

CSurface::CSurface() {
}

SDL_Surface* CSurface::OnLoad(char* File) {
    SDL_Surface* Surf_Temp = NULL;
    SDL_Surface* Surf_Return = NULL;

    if((Surf_Temp = IMG_Load(File)) == NULL) {
        return NULL;
    }

    //Surf_Return = SDL_DisplayFormatAlpha(Surf_Temp); //Returns a new surface
	Surf_Return = SDL_DisplayFormat(Surf_Temp); //Returns a new surface
    SDL_FreeSurface(Surf_Temp); //Clears the old surface so it doesn't use unnecessary memory

    return Surf_Return;
}

bool CSurface::OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int X, int Y) {
    if(Surf_Dest == NULL || Surf_Src == NULL) { // No destination? No source? Stop it.
        return false;
    }

    SDL_Rect DestR; //SDL_Rect has 4 members: x, y, w, h. Which makes us able to create rectangles if needed.

    DestR.x = X; //only X and Y, since we don't need width or height.
    DestR.y = Y;

    SDL_BlitSurface(Surf_Src, NULL, Surf_Dest, &DestR); //Finally we call the function to draw the image.

    return true;
}

bool CSurface::OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int X, int Y, int X2, int Y2, int W, int H) { //The function to copy a part from an image.
    if(Surf_Dest == NULL || Surf_Src == NULL) {
        return false;
    }

    SDL_Rect DestR;

    DestR.x = X;
    DestR.y = Y;

    SDL_Rect SrcR;

    SrcR.x = X2;
    SrcR.y = Y2;
    SrcR.w = W;
    SrcR.h = H;

    SDL_BlitSurface(Surf_Src, &SrcR, Surf_Dest, &DestR);

    return true;
}

bool CSurface::Transparent(SDL_Surface* Surf_Dest, int R, int G, int B) {
    if(Surf_Dest == NULL) { //If we don't have any destination (file) then shutdown.
        return false;
    }

    SDL_SetColorKey(Surf_Dest, SDL_SRCCOLORKEY | SDL_RLEACCEL, SDL_MapRGB(Surf_Dest->format, R, G, B)); //arguments: First is surface destination, second what flags it should have, and third the color it should remove.

    return true;
}

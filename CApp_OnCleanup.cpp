#include "CApp.h"

void CApp::OnCleanup() {    //The basic cleanup, here we'll put everything to be cleaned up.
    CArea::AreaControl.OnCleanup();

    for(int i = 0;i < (int)CEntity::EntityList.size();i++) {
        if(!CEntity::EntityList[i]) continue;

        CEntity::EntityList[i]->OnCleanup();
    }

    CEntity::EntityList.clear();

    CSoundBank::SoundControl.OnCleanup();
    Mix_CloseAudio();

    SDL_FreeSurface(Surf_Display);
    SDL_Quit();
}

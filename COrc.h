#ifndef _CORC_H_
    #define _CORC_H_

#include "CEntity.h"
#include <vector>

class COrc : public CEntity {
    public:
        COrc();

		bool OnLoad(char* File, int Width, int Height, int MaxFrames);

        void OnLoop();

        void OnRender(SDL_Surface* Surf_Display);

        void OnCleanup();

        void OnAnimate();

        bool OnCollision(CEntity* Entity);

        bool Fire();
};

#endif

#include "CProjectile.h"
#include "CApp.h"
#include "CTile.h"
#include <Windows.h>

CProjectile::CProjectile() {
    X = 0;
    Y = 0;

    Width = 0;
    Height = 0;

    MoveLeft = false;
    MoveRight = false;
    MoveUp = false;
    MoveDown = false;

    MaxSpeedX = 10;
    MaxSpeedY = 10;

    AccelX = MaxSpeedX;
    AccelY = MaxSpeedY;

    CurrentFrameCol = 0;
    CurrentFrameRow = 0;

    LifeTime = 0;
	FireDelay = 20;

	Interval = 0;

    Type = ENTITY_TYPE_PROJECTILE;
    Flags = ENTITY_FLAG_PLYDETECT;
}

bool CProjectile::OnLoad(char* File, int Width, int Height, int MaxFrames) {
    if(CEntity::OnLoad(File, Width, Height, MaxFrames) == false) {
        return false;
    }

    return true;
}

void CProjectile::OnLoop() {
    LifeTime += CFPS::FPSControl.GetSpeedFactor();
	Interval += CFPS::FPSControl.GetSpeedFactor();

	if(LifeTime > FireDelay)
	{
		Dead = true;
	}

    CEntity::OnLoop();
}

void CProjectile::OnRender(SDL_Surface* Surf_Display) {
    CEntity::OnRender(Surf_Display);
    if(Surf_Entity == NULL || Surf_Display == NULL) return;     //If we don't have anything to draw, then we skip this function

    CSurface::OnDraw(Surf_Display, Surf_Entity, X - CCamera::CameraControl.GetX(), Y - CCamera::CameraControl.GetY(), (CurrentFrameCol + Anim_Control.GetCurrentFrame())  * Width, (CurrentFrameRow) * Height, Width, Height);
}

void CProjectile::OnAnimate() {

    Anim_Control.OnAnimate();
}

bool CProjectile::OnCollision(CEntity* Entity) {
    if(Entity->Type == ENTITY_TYPE_ENEMY || Entity->Type == ENTITY_TYPE_PLAYER) {
        Dead = true;
    }

    return true;
}

void CProjectile::OnCleanup() {
    CEntity::OnCleanup();
}
